FROM quay.io/keycloak/keycloak:26.1.2-1@sha256:4704c51a25096e24379eafc81db5d7654a33b05e260a7eed2f4486b2daf00c45 as builder

ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true
ENV KC_FEATURES=token-exchange
ENV KC_DB=postgres
# Install custom providers
COPY keycloak-metrics-spi-6.0.0.jar /opt/keycloak/providers/
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:26.1.2-1@sha256:4704c51a25096e24379eafc81db5d7654a33b05e260a7eed2f4486b2daf00c45
COPY --from=builder /opt/keycloak/ /opt/keycloak/
WORKDIR /opt/keycloak
# change these values to point to a running postgres instance
#ENV KC_DB_URL=jdbc:postgresql://postgres:5432/keycloak
#ENV KC_DB_USERNAME=keycloak
#ENV KC_DB_PASSWORD=top-secret
ENV KC_HOSTNAME=auth.e13.dev
ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start", "--proxy-headers", "xforwarded", "--http-enabled", "true"]
